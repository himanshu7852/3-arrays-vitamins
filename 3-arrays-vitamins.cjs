const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

//1. Get all items that are available 

const getAllVitamins=items.filter((vitamins)=>{
    if(vitamins.available)
    {
        return vitamins;
    }
})
// console.log(getAllVitamins);

// 2. Get all items containing only Vitamin C.

const getItemsContainingVitC=items.filter((isVitaminC)=>{
    if(isVitaminC.contains.includes('Vitamin C'))
    {
        return isVitaminC;
    }
})

// console.log(getItemsContainingVitC);

// 3. Get all items containing Vitamin A.

const getItemsContainingVitA=items.filter((isVitaminA)=>{
    if(isVitaminA.contains.includes('Vitamin A'))
    {
        return isVitaminA;
    }
})

// console.log(getItemsContainingVitA);

/*4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        and so on for all items and all Vitamins.*/

const vitaminsContainItems=items.reduce((vitamins,item)=>{
    //let itemsConatinVitamin=[];
    if(vitamins.contains in vitamins)
    {
        item.contains.split(',').push(item.name);
    }   
    else{
        vitamins.contains=[];        
    } 
    return vitamins
},{})
console.log(vitaminsContainItems);


//5. Sort items based on number of Vitamins they contain.

const sortItemsBynumbeOfVitamins=items.sort((item1,item2)=>{
    let firstItem=item1.contains.split(',').length;
    let secondItem=item2.contains.split(',').length;
    return secondItem-firstItem;
})
// console.log(sortItemsBynumbeOfVitamins);